<?php

namespace app\components;

use Yii;
use yii\base\Component;
use yii\web\NotFoundHttpException;

class Beds24Api extends Component {

    const DAY_IN_SECONDS = 86400;
    /**
     * 
     * @param int $zhavroadRoomNumber
     * @param int $hotelId
     * @param Date (Y-m-d) $from
     * @param Date (Y-m-d) $to
     * @param float|null|false $priceRate false if price should not be changed  
     * @return array
     */
    public function getRoomsDatesFromZhavroad(int $zhavroadRoomNumber, int $hotelId, $from, $to, $priceRate): array {
        $this->__check_zhavroad_room_number_match_with_hotel($zhavroadRoomNumber, $hotelId);
        $target_id = $zhavroadRoomNumber;
        $resultQuery = [];
        for ($i = strtotime($from); $i <= strtotime($to); $i += self::DAY_IN_SECONDS) {
            $SQL = "SELECT `active`,`number`,`start_date`,`price` FROM bravo_hotel_room_dates WHERE start_date=:start_date AND target_id=:target_id";
            $resultQuery[] = Yii::$app->db_booking->createCommand($SQL)->bindValue(':start_date', date('Y-m-d', $i))->bindValue(':target_id', $target_id)->queryOne();
        }
        return $this->_changeArrayFormatForBeds24($resultQuery, $priceRate);
    }

    /**
     * 
     * @param array $array
     * @return array
     */
    private function _changeArrayFormatForBeds24(array $array, $priceRate): array {
        $arr = [];
        foreach ($array as $v) {
            $t = date('Ymd', strtotime($v['start_date']));
            if($priceRate !== false) {
                 $arr[$t]["p1"] = !is_null($priceRate) ? round((1 / $priceRate) * $v['price'], 2) :round($v['price'], 2);
            }
            $arr[$t]["i"] = $v['number'];
        }
        return $arr;
    }

    /**
     * update zhavroad OTA from beds24 API
     * @param array $apiResponse
     * @param int $zhavroadRoomNumber
     * @param int $hotelId
     * @param float|null|false $priceRate
     * @return array
     * @throws NotFoundHttpException
     */
    public function updateZhavroad(array $apiResponse, int $zhavroadRoomNumber, int $hotelId, $priceRate): array {
        $this->__check_zhavroad_room_number_match_with_hotel($zhavroadRoomNumber, $hotelId);
        $msg = [];
        foreach ($apiResponse as $time => $roomStatus) {
            $t = date("Y-m-d H:i:s", strtotime($time));
            $target_id = $zhavroadRoomNumber;
            if (!isset($roomStatus['i'])) {
                \Yii::error('inventory of room with date ' . trim(strip_tags(date("Y-m-d", strtotime($time)))) . ' is not set with hotel id: ' . $hotelId);
                throw new NotFoundHttpException('inventory of room with date ' . trim(strip_tags(date("Y-m-d", strtotime($time)))) . ' is not set with hotel id: ' . $hotelId);
            } elseif ($roomStatus['i'] > 0) {
                $number = $roomStatus['i'];
                $active = 1;
            } elseif ($roomStatus['i'] == 0) {
                $number = null;
                $active = 0;
            } else {
                \Yii::error('inventory number with ' . trim(strip_tags($time)) . ' is not correct, i= ' . trim(strip_tags($roomStatus['i'])) . 'with hotel id:' . $hotelId);
                throw new NotFoundHttpException('inventory number with ' . trim(strip_tags($time)) . ' is not correct, i= ' . trim(strip_tags($roomStatus['i'])) . 'with hotel id:' . $hotelId);
            }
            if (isset($roomStatus['p1']) && ($roomStatus['p1'] > 0)) {
                $beds24_price = $roomStatus['p1'];
                if($priceRate !== false){
                    $p = (!is_null($priceRate)) ? ($priceRate * $beds24_price) : $beds24_price;
                } else {
                    $p = null;
                }
            } else {
                $p = null;
            }
            if (is_null($p) && is_null($number)) {
                $SQL = "UPDATE bravo_hotel_room_dates SET active=:active WHERE start_date=:start_date AND target_id=:target_id";
                $resultQuery = Yii::$app->db_booking->createCommand($SQL)->bindValue(':start_date', $t)->bindValue(':target_id', $target_id)->bindValue(':active', $active)->execute();
            } elseif (is_null($p) && !is_null($number)) {
                $SQL = "UPDATE bravo_hotel_room_dates SET active=:active, number=:num WHERE start_date=:start_date AND target_id=:target_id";
                $resultQuery = Yii::$app->db_booking->createCommand($SQL)->bindValue(':start_date', $t)->bindValue(':num', $number)->bindValue(':target_id', $target_id)->bindValue(':active', $active)->execute();
            } elseif (!is_null($p) && is_null($number)) {
                $SQL = "UPDATE bravo_hotel_room_dates SET price=:price, active=:active WHERE start_date=:start_date AND target_id=:target_id";
                $resultQuery = Yii::$app->db_booking->createCommand($SQL)->bindValue(':start_date', $t)->bindValue(':price', $p)->bindValue(':target_id', $target_id)->bindValue(':active', $active)->execute();
            } elseif (!is_null($p) && !is_null($number)) {
                $SQL = "UPDATE bravo_hotel_room_dates SET price=:price, active=:active, number=:num WHERE start_date=:start_date AND target_id=:target_id";
                $resultQuery = Yii::$app->db_booking->createCommand($SQL)->bindValue(':start_date', $t)->bindValue(':num', $number)->bindValue(':price', $p)->bindValue(':target_id', $target_id)->bindValue(':active', $active)->execute();
            }
            if ($resultQuery == 0) {
                array_push($msg, "Row with time date of " . trim(strip_tags(date("Y-m-d", strtotime($time)))) . " and room number of $target_id dose not update");
            } else {
                array_push($msg, "Row with time date of " . trim(strip_tags(date("Y-m-d", strtotime($time)))) . " and room number of $target_id updated");
            }
        }
        return $msg;
    }
    /**
     * 
     * @param int $zhavroadRoomNumber
     * @param int $hotelId
     * @return type
     * @throws NotFoundHttpException
     */
    private function __check_zhavroad_room_number_match_with_hotel(int $zhavroadRoomNumber, int $hotelId) {
        $SQL = "SELECT `parent_id` FROM `bravo_hotel_rooms` WHERE id=:zhavroadRoomNumber";
        $zhavroad_room = Yii::$app->db_booking->createCommand($SQL)->bindValue(':zhavroadRoomNumber', $zhavroadRoomNumber)->queryOne();
        if ($zhavroad_room && ($zhavroad_room['parent_id'] == $hotelId)) {
            return;
        }
        throw new NotFoundHttpException('zhavroad room number dose not match with hotel');
    }
    /**
     * 
     * @param array $rooms
     * @return array
     * @throws NotFoundHttpException
     */
    public function getRoomNamesFromZhavroadOTA(array $rooms): array {
        $countRooms = count($rooms);
        $room_values = array_values($rooms);
        $newRoomsValues = [];
        $zhavroad_room_numbers = implode(', ', $room_values);
        $SQL = 'SELECT `id`,`title` FROM `bravo_hotel_rooms` WHERE `id` in (' . trim(strip_tags($zhavroad_room_numbers)) . ')';
        $zhavroad_room_titles = Yii::$app->db_booking->createCommand($SQL)->queryAll();
        if (empty($zhavroad_room_titles) || (count($zhavroad_room_titles) != $countRooms)) {
            throw new NotFoundHttpException('zhavroad room numbers do not match with saved room numbers in zhavroad OTA, check room numbers in zhavroad platform');
        }
        for ($i = 0; $i < $countRooms; $i++) {
            $key = array_search($zhavroad_room_titles[$i]['id'], $rooms);
            $newRoomsValues[$key] = sprintf('%s - room number(%s)', $zhavroad_room_titles[$i]['title'], $zhavroad_room_titles[$i]['id']);
        }
        return $newRoomsValues;
    }
    /**
     * extract price rate, zhavroad_price = rate * beds24_price
     * @param array $beds24_information
     * @param int $roomId beds24 room id ex, 236035
     * @return float|null|false
     */
    public function getPriceRate(array $beds24_information, ?int $roomId) {
        if ($beds24_information['change_price'] == 'no') {
            return false;
        }
        if (is_null($roomId)) {
            return null;
        }
        if($beds24_information['apply_rooms_price_rate'] == 'no'){
            // return 1 as a price rate if apply_rooms_price_rate is not set
            return 1;
        }
        if (isset($beds24_information['rooms_price_rates'])) {
            $rooms_price_rates = $beds24_information['rooms_price_rates'];
            return $rooms_price_rates[$roomId] ?? null;
        }
        return null;
    }
}
