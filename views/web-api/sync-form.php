<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\date\DatePicker;
use kartik\select2\Select2;

$this->title = 'Sync api for Beds24 and Zhavroad';
$this->params['breadcrumbs'][] = ['label' => 'Hotel Apis', 'url' => ['/hotel-apis/index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="sync-form">
   <div class="col-md-6">
    <?php $form = ActiveForm::begin(); ?>
        <?php
        echo '<label class="control-label">From and To</label>';
        echo DatePicker::widget([
            'model' => $model,
            'attribute' => 'from',
            'attribute2' => 'to',
            'options' => ['placeholder' => 'From'],
            'options2' => ['placeholder' => 'To'],
            'type' => DatePicker::TYPE_RANGE,
            'form' => $form,
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'autoclose' => true,
            ]
        ]);
     ?>
    <?= $form->field($model, 'roomId')->widget(Select2::classname(), [
        'data' => $rooms,
        'options' => ['placeholder' => 'Select room number from Zhavroad OTA'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Get from Beds24', ['class' => 'btn btn-success','name'=>'get-from-beds24']) ?>
        
        <?= Html::submitButton('Set to Beds24', ['class' => 'btn btn-info','name'=>'set-to-Beds24']) ?>
    </div>

    <?php ActiveForm::end(); ?>
   </div>
</div>

