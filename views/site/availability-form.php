<?php
/* @var $success app\controllers\SiteController actionAvailability*/
/* @var $msgs app\controllers\SiteController actionAvailability */
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Availability booking form';
$this->params['breadcrumbs'][] = $this->title;
?>
<h2><?= Html::encode($this->title) ?></h2>
<?php
if(!empty($msgs)){
    $str = "<div class='alert alert-danger'>";
    $str .= "<ul>";
    foreach ($msgs as $msg){
       $str .= "<li>".Html::encode($msg)."</li>";
    }
    $str .="</ul>";
    $str .= "</div>";
    echo $str;
}else { ?>
<div class="row">
    <p style="padding-left: 12px;" class="alert alert-warning">You must upload a file in the following format, ex download from this <a href="/uploads/csv-format-example.csv">link</a></p>
    <div class="col-lg-5">
        <?php $form = ActiveForm::begin([
                'id' => 'booking-form'
        ]) ?>
        <?= Html::errorSummary($model) ?>
        <?= $form->field($model, 'apiKey') ?>
        <?= $form->field($model, 'file')->fileInput() ?>
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'availability-button']) ?>
        </div>
        <?php ActiveForm::end() ?>
    </div>
</div>
<?php } ?>