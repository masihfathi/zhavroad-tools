<?php
use yii\helpers\Html;

$this->title = 'Channel Manager Form';
$this->params['breadcrumbs'][] = $this->title;
?>
<h2><?= Html::encode($this->title) ?></h2>

<div class="row">
    <div class="col-lg-5">
        <?= Html::beginForm('https://admin.zhavroad.com/control2.php') ?>
        <label class="control-label" for="username">Username</label>
        <input type='text' class="form-control" name='username'>
        <div class="help-block"></div>
        <label class="control-label" for="loginpass">Password</label>
        <input type='password' class="form-control" name='loginpass'>
        <div class="help-block"></div>
        <input type="hidden" name="subbookid" value="">
        <input type="hidden" name="sublogin" value="1">
        <input type='submit' class="btn btn-primary" name='submit' value='Submit'>
        <?= Html::endForm() ?>
    </div>
</div>
