<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\HotelApis */

$this->title = 'Create Hotel Apis';
$this->params['breadcrumbs'][] = ['label' => 'Hotel Apis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hotel-apis-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
