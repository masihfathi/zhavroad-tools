<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\models\HotelApisSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Hotel Apis';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hotel-apis-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Hotel Apis', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'hotel_id',
                'headerOptions' => ['style' => 'width:5%'],
            ],
            [
                'attribute' => 'hotel_name',
                'headerOptions' => ['style' => 'width:10%'],
            ],
            'api_key',
            [
                'attribute' => 'beds24_room_map',
                'headerOptions' => ['style' => 'width:35%'],
            ],
             [
                'attribute' => 'status',
                'value' => function ($data) {
                    return ucfirst($data->status); 
                },
            ],
            [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view} {update} {delete} {sync}',  
                'buttons' => [
                    'sync' => function($url, $model, $key) {     
                        return Html::a('', Url::to(["/web-api/sync",'id'=>$model->hotel_apis_id], true),["class"=>"glyphicon glyphicon-triangle-right"]);
                    }
                ]
            ]
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
