<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\HotelApis */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="hotel-apis-form">
    <div id="msg">
    </div>
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'hotel_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hotel_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'api_key')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'beds24_room_map')->textarea(['rows' => '12']) ?>
    
    <?= $form->field($model, 'status')->dropDownList(
            [
                'active'=> Yii::t('app', 'Active'),
                'inactive'=> Yii::t('app', 'Inactive'),
            ]
            ) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        <?php
        if (!$model->isNewRecord) {
            ?>
            <?= Html::a('Test Connection', '#', ['class' => 'btn btn-info', "id" => "connection-test"]) ?>
            <?php
        }
        ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$url = Url::to(['web-api/test-connection','id'=> $model->hotel_apis_id ],true);
$JS = <<<EOL
    $(document).ready(function () {
        $(document).ajaxStart(function(){
            $("#connection-test").attr("disabled", true);
        });
        $(document).ajaxComplete(function(){
            $("#connection-test").attr("disabled", false);
        });
        $("#connection-test").on("click", function(){
            $.ajax({
                type: "GET",
                url: "$url",
                success: function (data) {
                    if(data.success == true){
                        $('#msg').html(
                            `<div class="alert alert-success" role="alert">
                                Connection information is correct
                            </div>`
                        );
                    }else{
                        $('#msg').html(
                            `<div class="alert alert-danger" role="alert">
                                Connection information is not correct!
                            </div>`
                        );
                    }
                },
                error: function () {
                    $('#msg').html(
                        `<div class="alert alert-danger" role="alert">
                                Connection information is not correct!
                            </div>`
                    );
                }
            });
        });
    });
EOL;
if(!$model->isNewRecord) {
    $this->registerJs(
        $JS,
        $this::POS_READY
    );
}
?>