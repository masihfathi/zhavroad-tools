<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\HotelApisSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="hotel-apis-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'hotel_apis_id') ?>

    <?= $form->field($model, 'hotel_id') ?>

    <?= $form->field($model, 'hotel_name') ?>

    <?= $form->field($model, 'api_key') ?>

    <?= $form->field($model, 'beds24_room_map') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
