<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\HotelApis */

$this->title = 'Update Hotel Apis: ' . $model->hotel_name;
$this->params['breadcrumbs'][] = ['label' => 'Hotel Apis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->hotel_name, 'url' => ['view', 'id' => $model->hotel_apis_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="alert alert-info">
    (zhavroad price) = (price rate) * (beds24 price)
</div>
<div class="hotel-apis-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
