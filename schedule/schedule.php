<?php

namespace app\schedule;

use Yii;
use yii\caching\TagDependency;
use app\models\SyncForm;

/**
 * @var \omnilight\scheduling\Schedule $schedule
 */
// call this task for each room
// $roomId = $beds24_room_id;
/*
  ex,
  $rooms = [
  "236037" => "47",
  "236035 "=> "45",
  "236036"=>"46"
  ]
 */
$db = \Yii::$app->db;

$duration = 3600;     // cache query results for 3600 seconds.

$result = $db->cache(function ($db) {

    $SQL = 'SELECT `hotel_id`, `beds24_room_map`,`status` FROM hotel_apis WHERE JSON_EXTRACT(`beds24_room_map`, "$.auto_sync") = :auto_sync';

    return $db->createCommand($SQL)->bindValue(':auto_sync', 'yes')->queryAll();
}, $duration, new TagDependency(['tags' => 'auto_sync_schedule']));

foreach ($result as $value) {
    if ($value['status'] == 'active') {
        $beds24_information = json_decode($value['beds24_room_map'], true);

        $auto_sync_time = $beds24_information['auto_sync_time']; // auto sync time in minutes

        $hotel_id = (int) $value['hotel_id'];
        $schedule->call(function() use($beds24_information, $hotel_id) {
            $rooms = $beds24_information['rooms'];
            foreach ($rooms as $roomId => $zhavroadRoomNumber) {
                $priceRate = \Yii::$app->Beds24Api->getPriceRate($beds24_information, $roomId);
                $model = new SyncForm(['scenario' => SyncForm::SCENARIO_WEBHOOK_FOR_ZHAVROAD_SYNC]);
                $model->roomId = $roomId;
                $from = date('Ymd', time());
                $to = date('Ymd', strtotime('+1 year'));
                $res = $model->Beds24GetRoomDates($beds24_information, $from, $to);
                if ($res['success'] === true) {
                    $msg = \Yii::$app->Beds24Api->updateZhavroad($res['info'], $zhavroadRoomNumber, $hotel_id, $priceRate);
                    \Yii::error($msg);
                } else {
                    \Yii::error($res);
                }
                sleep(5); // 5 second sleep between each rooms set
            }
        })->everyNMinutes($auto_sync_time);
    }
}


