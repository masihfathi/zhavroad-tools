<?php 

namespace app\models;
use GuzzleHttp\Client;

class SyncForm extends \yii\base\Model {
    
    const SCENARIO_WEBHOOK_FOR_ZHAVROAD_SYNC = 'webhook-for-zhavroad-sync';

    public $from;

    public $to;

    public $roomId;
    /**
     * 
     * @return array
     */
    public function rules():array {
        return [
            [['from','to','roomId'], 'required'],
            [['from','to','roomId'], 'safe'],
            ['from', 'fromValidator'],
            ['to', 'toValidator'],
            ['to', 'toValidator'],
            [['roomId'], 'required', 'on' => self::SCENARIO_WEBHOOK_FOR_ZHAVROAD_SYNC],
        ];
    }
    public function fromValidator($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $date_now = date("yyyy-mm-dd"); 
            $from = date("yyyy-mm-dd", strtotime($this->from));
            if($date_now > $from) {
                $this->addError($attribute, 'From field must be greater than today');
            }
        }
    }
    public function toValidator($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $to = date("yyyy-mm-dd",strtotime($this->to)); 
            $from = date("yyyy-mm-dd",strtotime($this->from));
            if($from > $to) {
                $this->addError($attribute, 'To field must be greater than from field');
            }
        }
    }
     /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'from' => 'From',
            'to' => 'To',
            'roomId' => 'Room Number',
        ];
    }
    /**
     * 
     * @param array $beds24_information
     * @return type
     */
    public function syncFromBeds24(array $beds24_information) {
        if($this->validate()) {
            $from = str_replace("-", "", $this->from);
            $to = str_replace("-", "", $this->to);
            return $this->Beds24GetRoomDates($beds24_information,$from,$to);
        }
        return $this->errors;
    }
    /**
     * 
     * @param array $beds24_information
     * @param array $zhavroadRoomsAvailibility
     * @return array
     */
    public function Beds24SetRoomDates(array $beds24_information, array $zhavroadRoomsAvailibility):array {
        $authentication = $beds24_information['beds24_authentication'];
        $auth = array();
        $auth['apiKey'] = $authentication['apiKey'];
        $auth['propKey'] = $authentication['propKey'];
        $data = array();
        $data['authentication'] = $auth;
        if(!empty($this->roomId)){
            $data['roomId'] = $this->roomId;
        }
        if(!isset($beds24_information['propId'])){
            return ['success' => false, "error" => "propId is not set", "errorCode" => 1000];
        }
        $data['propId'] = $beds24_information['propId'];        
        // get dates status for hotel from zhavroad
        $data['dates'] = $zhavroadRoomsAvailibility;
        $client = new Client();
        $res = $client->request('POST', 'https://api.beds24.com/json/setRoomDates', [
            'body' => json_encode($data)
        ]);
        return $this->__extractData($res);
        
    }
    /**
     * 
     * @param array $beds24_information
     * @param Date("Ymd") $from ex date("Ymd",time()) = 20200225
     * @param Date("Ymd") $to ex date('Ymd', strtotime('+1 year')) = 20210225
     * @return array
     */
    public function Beds24GetRoomDates($beds24_information,$from,$to) {
        $authentication = $beds24_information['beds24_authentication'];
        $auth = array();
        $auth['apiKey'] = $authentication['apiKey'];
        $auth['propKey'] = $authentication['propKey'];
        $data = array();
        $data['authentication'] = $auth;
        if(!empty($this->roomId)){
            $data['roomId'] = $this->roomId;
        }
        if(!isset($beds24_information['propId'])){
            return ['success' => false, "error" => "propId is not set", "errorCode" => 1000];
        }
        $data['propId'] = $beds24_information['propId'];
        $data['from'] = $from;
        $data['to'] = $to;

        $client = new Client();
        $res = $client->request('POST', 'https://api.beds24.com/json/getRoomDates', [
            'body' => json_encode($data)
        ]);
        return $this->__extractData($res);

    }
    private function __extractData($res):array {
        if ($res->getStatusCode() == 200) {
            $result = $res->getBody()->getContents();
            $info = json_decode($result, true);
            if (isset($info['error'])) {
                $info['success'] = false;
                return $info;
            } else {
                return ['success' => true, "error" => "", "errorCode" => "",'info'=>$info];
            }
        }else {
            return ['success' => false, "error" => "", "errorCode" => $res->getStatusCode()];
        }        
    }
}