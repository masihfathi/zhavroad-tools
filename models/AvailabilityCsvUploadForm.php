<?php
namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class AvailabilityCsvUploadForm extends Model {
    public $apiKey;
    /**
     * @var UploadedFile file attribute
     */
    public $file;
    /**
     * @var $hotel_api_obj  \app\models\HotelApis
     */
    public $hotel_api_obj = null;
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['file', 'required'],
            [['apiKey'], 'safe'],
            ['apiKey','apiKeyValidation'],
            ['file', 'file','extensions' => 'csv', 'checkExtensionByMimeType' => false],
        ];
    }
    public function apiKeyValidation($attribute_name, $params)
    {
        $this->hotel_api_obj = \app\models\HotelApis::find()->where(['api_key' => $this->apiKey])->one();
        if (empty($this->apiKey) || is_null($this->hotel_api_obj)) {
            $this->addError($attribute_name, 'Api key is not correct');
            return false;
        }
        return true;
    }
    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'file' => 'File',
            'apiKey' => 'Api Key',
        ];
    }
    public function sendEmailToAdmin()
    {
        Yii::$app->mailer->compose()
            ->setTo(Yii::$app->params['adminEmail'])
            ->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->params['senderName']])
            ->setSubject('Availability Csv File Uploaded')
            ->setTextBody(
                'Api Key: '. $this->apiKey
            )
            ->send();
    }
}