<?php

namespace app\models;

use Yii;
use yii\caching\TagDependency;
/**
 * This is the model class for table "hotel_apis".
 *
 * @property int $hotel_apis_id
 * @property string $hotel_id
 * @property string|null $hotel_name
 * @property string|null $api_key
 * @property string|null $beds24_room_map
 */
class HotelApis extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'hotel_apis';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['hotel_id','status'], 'required'],
            [['hotel_id', 'hotel_name', 'api_key'], 'string', 'max' => 255],
            [['beds24_room_map','status'], 'safe'],
            [['beds24_room_map'], 'beds24RoomMapValidator'],
        ];
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if (empty($this->beds24_room_map)) {
                $this->beds24_room_map = '{}';
            }
            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes) {
        if (parent::afterSave($insert, $changedAttributes) && !empty($changedAttributes)) {
            TagDependency::invalidate(Yii::$app->cache, 'auto_sync_schedule');
            return true;
        }
        return false;
    }

    public function beds24RoomMapValidator($attribute, $params) {
        if (empty($this->beds24_room_map) || ($this->beds24_room_map == '{}')) {
            return false;
        }
        if (($a = json_decode($this->beds24_room_map, true)) == false) {
            $this->addError($attribute, 'json format is not correct');
        }
        if (!isset($a['name'])) {
            $this->addError($attribute, 'name field property is not set in the beds24_room_map field');
        }
        if (!isset($a['description'])) {
            $this->addError($attribute, 'description field property is not set in the beds24_room_map field');
        }
        if (!isset($a['beds24_authentication'])) {
            $this->addError($attribute, 'beds24_authentication field property is not set in the beds24_room_map field');
        }
        if (!isset($a['propId'])) {
            $this->addError($attribute, 'propId field property is not set in the beds24_room_map field');
        }
        if (!isset($a['ownerId'])) {
            $this->addError($attribute, 'ownerId field property is not set in the beds24_room_map field');
        }
        if (!isset($a['rooms'])) {
            $this->addError($attribute, 'rooms field property is not set in the beds24_room_map field');
        }
        if (!isset($a['apply_rooms_price_rate'])) {
            $this->addError($attribute, 'apply_rooms_price_rate field property is not set in the beds24_room_map field');
        }
        if (!isset($a['change_price'])) {
            $this->addError($attribute, 'change_price field property is not set in the beds24_room_map field');
        }
        if (!isset($a['rooms_price_rates'])) {
            $this->addError($attribute, 'rooms_price_rates field property is not set in the beds24_room_map field');
        }
        if (!isset($a['auto_sync'])) {
            $this->addError($attribute, 'auto_sync field property is not set in the beds24_room_map field');
        }
        if (isset($a['auto_sync']) && ( $a['auto_sync'] != 'yes') && ( $a['auto_sync'] != 'no')) {
            $this->addError($attribute, 'auto_sync field property must be yes/no');
        }
        if (!isset($a['auto_sync_time'])) {
            $this->addError($attribute, 'auto_sync_time field property is not set in the beds24_room_map field');
        }
        if (isset($a['auto_sync_time']) && (($a['auto_sync_time'] < 0))) {
            $this->addError($attribute, 'auto_sync_time field property must be greater than zero');
        }
        if (isset($a['auto_sync_time']) && !is_int($a['auto_sync_time'])) {
            $this->addError($attribute, 'auto_sync_time field property must be integer value(the value is in the minite unit)');
        }
        if (isset($a['apply_rooms_price_rate']) && ( $a['apply_rooms_price_rate'] != 'yes') && ( $a['apply_rooms_price_rate'] != 'no')) {
            $this->addError($attribute, 'apply_rooms_price_rate field property must be yes/no');
        }
        if (isset($a['change_price']) && ( $a['change_price'] != 'yes') && ( $a['change_price'] != 'no')) {
            $this->addError($attribute, 'change_price field property must be yes/no');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'hotel_apis_id' => 'Hotel Apis ID',
            'hotel_id' => 'Hotel ID',
            'hotel_name' => 'Hotel Name',
            'api_key' => 'Api Key',
            'beds24_room_map' => 'Beds24 Room Map',
        ];
    }

}
