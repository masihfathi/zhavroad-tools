<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\HotelApis;

/**
 * HotelApisSearch represents the model behind the search form of `app\models\HotelApis`.
 */
class HotelApisSearch extends HotelApis
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hotel_apis_id'], 'integer'],
            [['hotel_id', 'hotel_name', 'api_key'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = HotelApis::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'hotel_apis_id' => $this->hotel_apis_id,
        ]);

        $query->andFilterWhere(['like', 'hotel_id', $this->hotel_id])
            ->andFilterWhere(['like', 'hotel_name', $this->hotel_name])
            ->andFilterWhere(['like', 'api_key', $this->api_key]);

        return $dataProvider;
    }
}
