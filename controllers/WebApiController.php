<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use GuzzleHttp\Client;
use yii\helpers\Json;
use app\models\HotelApis;
use app\models\SyncForm;
use yii\helpers\Html;

class WebApiController extends Controller {

    /**
     * @return array
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['sync-to-zhavroad-by-webhook'], // add all actions to take guest to login page
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'sync-to-zhavroad-by-webhook' => ['POST']
                ],
            ],
        ];
    }
    public function beforeAction($action)
    {
        if (in_array($action->id, ['sync-to-zhavroad-by-webhook'])) {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }
    /**
     * @param int $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionTestConnection(int $id) {
        $model = $this->findModel($id);
        if (!empty($model->beds24_room_map)) {
            $beds24_information = Json::decode($model->beds24_room_map, true);
            if (!isset($beds24_information['beds24_authentication'])) {
                return $this->asJson(['success' => false, "error" => "", "errorCode" => ""]);
            }
            $authentication = $beds24_information['beds24_authentication'];
            $auth = array();
            $auth['apiKey'] = $authentication['apiKey'];
            $auth['propKey'] = $authentication['propKey'];
            $data = array();
            $data['authentication'] = $auth;
            $client = new Client();
            $res = $client->request('POST', 'https://api.beds24.com/json/getPropertyContent', [
                'body' => json_encode($data)
            ]);
            if ($res->getStatusCode() == 200) {
                $result = $res->getBody()->getContents();
                $info = json_decode($result, true);
                if (isset($info['error'])) {
                    $info['success'] = false;
                    return $this->asJson($info);
                } else {
                    return $this->asJson(['success' => true, "error" => "", "errorCode" => ""]);
                }
            } else {
                return $this->asJson(['success' => false, "error" => "", "errorCode" => $res->getStatusCode()]);
            }
        } else {
            return $this->asJson(['success' => false, "error" => "", "errorCode" => ""]);
        }
    }
    
    /**
     * Sync zhavroad platform with beds24
     * @param int $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionSync(int $id) {
        $hotel_api = $this->findModel($id);
        if($hotel_api->status == 'inactive') {
            throw new NotFoundHttpException('The property is inactive');
        }
        $beds24_room_map = $hotel_api->beds24_room_map;
        $rooms = [];
        $res = [];
        $model = new SyncForm;
        if (!empty($beds24_room_map)) {
            $beds24_information = Json::decode($hotel_api->beds24_room_map, true);
            if (!isset($beds24_information['beds24_authentication'])) {
                throw new NotFoundHttpException('authentication information is not set!');
            }
            if (!isset($beds24_information['rooms'])) {
                throw new NotFoundHttpException('rooms information is not complete!');
            }
            $rooms = $beds24_information['rooms'];
            $newRoomsValues = Yii::$app->Beds24Api->getRoomNamesFromZhavroadOTA($rooms);
            if ($model->load(Yii::$app->request->post())) {
                if (isset($_POST['get-from-beds24'])) {
                    return $this->_getFromBeds24($rooms, $model, $beds24_information, $hotel_api);
                } elseif (isset($_POST['set-to-Beds24']) && $model->validate()) {
                    return $this->_setToBeds24($rooms, $model, $beds24_information, $hotel_api);
                }
            }
            return $this->render('sync-form', ['model' => $model, 'rooms' => $newRoomsValues]);
        }
        throw new NotFoundHttpException('beds24 room map field is not set!');
    }
    /**
     * action to sync availability from beds24 to Zhavroad when beds24 webhook call
     */
    public function actionSyncToZhavroadByWebhook() {
        foreach (Yii::$app->request->post() as $k=>$v){
            $post = json_decode($k,true);
        }
        if(!empty($post) && isset($post['roomId']) && isset($post['propId']) && isset($post['ownerId'])){
            $roomId = $post['roomId'];
            $propId = $post['propId'];
            $ownerId = $post['ownerId'];
            $SQL = 'SELECT `hotel_id`, `beds24_room_map`,`status` FROM hotel_apis WHERE JSON_EXTRACT(`beds24_room_map`, "$.propId","$.ownerId") = JSON_ARRAY(:prop_id,:ownerId)';
            $resultQuery = Yii::$app->db->createCommand($SQL)->bindValue(':prop_id', $propId)->bindValue(':ownerId', $ownerId)->queryOne();
            if($resultQuery != 0 && ($resultQuery['status'] == 'active') ){          
                $beds24_information = json_decode($resultQuery['beds24_room_map'],true);
                $rooms = $beds24_information['rooms'];
                $zhavroadRoomNumber = $rooms[$roomId];
                $priceRate = Yii::$app->Beds24Api->getPriceRate($beds24_information, $roomId);
                $model = new SyncForm(['scenario' => SyncForm::SCENARIO_WEBHOOK_FOR_ZHAVROAD_SYNC]);
                $model->roomId = $roomId;
                $from = date('Ymd', time());
                $to = date('Ymd', strtotime('+1 year'));
                $res = $model->Beds24GetRoomDates($beds24_information, $from, $to);
                if($res['success'] === true) {
                    $msg = Yii::$app->Beds24Api->updateZhavroad($res['info'], $zhavroadRoomNumber, $resultQuery['hotel_id'], $priceRate);
                    Yii::error($msg);
                }else{
                    Yii::error($res);
                }
            }else{
                Yii::error('have error in sql with propId '. Html::encode($propId). ' and owenerId '. Html::encode($ownerId) . 'or the property is inactive');
            }
        }
    }
    /**
     * 
     * @param array $rooms
     * @param SyncForm $model
     * @param array $beds24_information
     * @param HotelApis $hotel_api
     * @return \yii\web\Response
     */
    private function _getFromBeds24(array $rooms, SyncForm $model, array $beds24_information, HotelApis $hotel_api) {
        $res = $model->syncFromBeds24($beds24_information);
        // sync form result
        return $this->_syncFormResult($res, $rooms, $model, $beds24_information, $hotel_api);
    }
    /**
     * 
     * @param array $rooms
     * @param SyncForm $model
     * @param array $beds24_information
     * @param HotelApis $hotel_api
     * @return \yii\web\Response
     */
    private function _setToBeds24(array $rooms,SyncForm $model,array $beds24_information, HotelApis $hotel_api) {
        $zhavroadRoomNumber = $rooms[$model->roomId];
        $priceRate = Yii::$app->Beds24Api->getPriceRate($beds24_information, $model->roomId);
        $zhavroadRoomsAvailibility = Yii::$app->Beds24Api->getRoomsDatesFromZhavroad($zhavroadRoomNumber,$hotel_api->hotel_id,$model->from,$model->to,$priceRate);
        $msg = $model->Beds24SetRoomDates($beds24_information,$zhavroadRoomsAvailibility);
        return $this->render('sync-form-result', ['model' => $model, 'res' => $msg]);        
    }
    /**
     * SyncForm model load result 
     * @param type $res
     * @param type $rooms
     * @param type $model
     * @param type $beds24_information
     * @param type $hotel_api
     * @return \yii\web\Response
     */
    private function _syncFormResult($res, $rooms, $model, $beds24_information, $hotel_api) {
        if (isset($res['success']) && $res['success']) {
            $zhavroadRoomNumber = $rooms[$model->roomId];
            $priceRate = Yii::$app->Beds24Api->getPriceRate($beds24_information, $model->roomId);
            $msg = Yii::$app->Beds24Api->updateZhavroad($res['info'], $zhavroadRoomNumber, $hotel_api->hotel_id, $priceRate);
            return $this->render('sync-form-result', ['model' => $model, 'res' => $msg]);
        }
        return $this->render('sync-form-result', ['model' => $model, 'res' => $res]);
    }

    /**
     * Finds the HotelApis model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return HotelApis the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(int $id) {
        if (($model = HotelApis::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
