<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\db\Query;
use yii\helpers\ArrayHelper;

use app\models\LoginForm;
use app\models\AvailabilityCsvUploadForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                //'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
    
    public function actionAvailability()
    {
        $model = new AvailabilityCsvUploadForm();
        if ($model->load(Yii::$app->request->post())) {
            $model->file = UploadedFile::getInstance($model, 'file');
            if ($model->validate() && $model->file) {
                $filePath = 'uploads/' . $model->file->baseName.uniqid() . '.' . $model->file->extension;
                $model->file->saveAs($filePath);
                $msgs = $this->_editBooking($filePath,$model);
                unlink($filePath);
                return $this->render('availability-form',[
                    'msgs' => $msgs
                ]);
            }else {
                return $this->render('availability-form',[
                    'model' => $model,
                    'msgs' => ''
                ]);
            }
        }else {
            return $this->render('availability-form', [
                'model' => $model,
                'msgs' => ''
            ]);
        }
    }

    private function _editBooking($filePath,$model)
    {
        $handle = fopen($filePath, "r");
        $j = 0;
        $time = [];
        $roomNumber = [];
        $capacity = [];
        $price = [];
        $fileHeader = [];
        while (($fileOp = fgetcsv($handle, 1000, ",")) !== false)
        {
            if($j == 0){
                $fileHeader = $fileOp;
                $j++;
                continue;
            }
            $totalColumn = count($fileOp);
            for ($i=1; $i < $totalColumn;$i+=2){
                $time[] = $fileOp[0];
                $roomNumber[] = $fileHeader[$i];
                $capacity[] = $fileOp[$i];
                $price[] = $fileOp[$i+1];
            }
        }
        $rows = (new Query())
            ->select(['parent_id'])
            ->from('bravo_hotel_rooms')
            ->where(['in', 'id', $roomNumber])
            ->all(Yii::$app->db_booking);
        $success = true;
        $hotel_id = $model->hotel_api_obj->hotel_id;
        if(!empty($rows)){
            $hotel_ids = ArrayHelper::getColumn($rows, 'parent_id');
            foreach ($hotel_ids as $value) {
                if($value != $hotel_id) {
                    $success = false;
                }
            }
            if ($success === true) {
                return $this->_updateBookingDb($time, $roomNumber, $capacity, $price);
            }
        }
        return ['room numbers are incorrect'];
    }
    private function _updateBookingDb($time,$roomNumber,$capacity,$price)
    {
        $total = count($time);
        $msg = [];
        for($i=0;$i<$total;$i++) {
            $t = date("Y-m-d H:i:s",strtotime($time[$i]));
            $target_id = $roomNumber[$i];
            if($price[$i] > 0) {
                $p = $price[$i];
            }else {
                $p = null;
            }
            if($capacity[$i] == 0) {
                $active = 0;
                $number = null;
            }elseif($capacity[$i] > 0) {
                $active = 1;
                $number = $capacity[$i];
            }else {
                $msg[] = "Row with time date of $time[$i] and room number of $roomNumber[$i] dose not update";
                continue;
            }
            $resultQuery = 0;
            if(is_null($p) && is_null($number)){
                $SQL = "UPDATE bravo_hotel_room_dates SET active=:active WHERE start_date=:start_date AND target_id=:target_id";
                $resultQuery = Yii::$app->db_booking->createCommand($SQL)->bindValue(':start_date', $t)->bindValue(':target_id', $target_id)->bindValue(':active', $active)->execute();
            }elseif(is_null($p) && !is_null($number)) {
                $SQL = "UPDATE bravo_hotel_room_dates SET active=:active, number=:num WHERE start_date=:start_date AND target_id=:target_id";
                $resultQuery = Yii::$app->db_booking->createCommand($SQL)->bindValue(':start_date', $t)->bindValue(':num', $number)->bindValue(':target_id', $target_id)->bindValue(':active', $active)->execute();
            }elseif(!is_null($p) && is_null($number)) {
                $SQL = "UPDATE bravo_hotel_room_dates SET price=:price, active=:active WHERE start_date=:start_date AND target_id=:target_id";
                $resultQuery = Yii::$app->db_booking->createCommand($SQL)->bindValue(':start_date', $t)->bindValue(':price', $p)->bindValue(':target_id', $target_id)->bindValue(':active', $active)->execute();
            }elseif(!is_null($p) && !is_null($number)){
                $SQL = "UPDATE bravo_hotel_room_dates SET price=:price, active=:active, number=:num WHERE start_date=:start_date AND target_id=:target_id";
                $resultQuery = Yii::$app->db_booking->createCommand($SQL)->bindValue(':start_date', $t)->bindValue(':num', $number)->bindValue(':price', $p)->bindValue(':target_id', $target_id)->bindValue(':active', $active)->execute();
            }
            if($resultQuery == 0){
                array_push($msg, "Row with time date of $time[$i] and room number of $roomNumber[$i] dose not update");
            }else {
                array_push($msg, "Row with time date of $time[$i] and room number of $roomNumber[$i] updated");
            }
        }
        if(empty($msg)) {
            array_push($msg, 'File is empty');
        }
        return $msg;
    }
    public function actionChannelManager(){
        return $this->render('channel-form');
    }
}
