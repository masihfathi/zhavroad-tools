<?php
namespace tests\unit;

use app\models\SyncForm;

class actionSyncToZhavroadByWebhookTest extends \Codeception\Test\Unit {

    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before() {
        
    }

    protected function _after() {
        
    }

    // tests
    /**
     * @covers WebApiController::actionSyncToZhavroadByWebhook
     */
    public function testSyncByWebhook() {
        $roomId = "243948";
        $propId = "107406";
        $ownerId = "60199";
        $SQL = 'SELECT `hotel_id`, `beds24_room_map`,`status` FROM hotel_apis WHERE JSON_EXTRACT(`beds24_room_map`, "$.propId","$.ownerId") = JSON_ARRAY(:prop_id,:ownerId)';
        $resultQuery = \Yii::$app->db->createCommand($SQL)->bindValue(':prop_id', $propId)->bindValue(':ownerId', $ownerId)->queryOne();
        $this->assertNotFalse($resultQuery);
        $this->assertSame('active', $resultQuery['status']);
        if ($resultQuery != 0  && ($resultQuery['status'] == 'active')) {
            $beds24_information = json_decode($resultQuery['beds24_room_map'], true);
            $rooms = $beds24_information['rooms'];
            $zhavroadRoomNumber = $rooms[$roomId];
            $this->assertEquals(50, $zhavroadRoomNumber);
            $priceRate = \Yii::$app->Beds24Api->getPriceRate($beds24_information, $roomId);
            $this->assertEquals(1.4, $priceRate);
            $model = new SyncForm(['scenario' => SyncForm::SCENARIO_WEBHOOK_FOR_ZHAVROAD_SYNC]);
            $model->roomId = $roomId;
            $from = date('Ymd', time());
            $to = date('Ymd', strtotime('+1 year'));
            $res = $model->Beds24GetRoomDates($beds24_information, $from, $to);
            $this->assertTrue($res['success']);
            if ($res['success'] === true) {
                $msg = \Yii::$app->Beds24Api->updateZhavroad($res['info'], $zhavroadRoomNumber, $resultQuery['hotel_id'], $priceRate);
                $this->assertEquals(366, count($msg));
            }
        }else {
            $this->assertSame('inactive', $resultQuery['status']);
        }
    }

}
