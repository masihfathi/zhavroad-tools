<?php
namespace tests\unit;

class getPriceRateTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    
    private $_roomId;
    
    private $_beds24_information1;
    private $_beds24_information2;
    
    protected function _before()
    {
        $this->_roomId = 236035;
        $this->_beds24_information1 = json_decode(''
                . '{
	"name": "beds24 api",
	"description": "amin abad property",
	"beds24_authentication": {
		"apiKey": "323eee33464f4981",
		"propKey": "323eee33464f4982"               
	},
        "ownerId": "58212",
        "propId": "103724",
        "auto_sync":"yes",
        "auto_sync_time":2,
	"rooms": {
		"236037": "47",
		"236035": "45",
		"236036": "46"
	},
       "change_price":"yes",
       "apply_rooms_price_rate":"yes",
       "rooms_price_rates":{
                "236037": "1.4",
		"236035": "4",
		"236036": "1.2"
        }
}',true);
        $this->_beds24_information2 = json_decode(''
                . '{
	"name": "beds24 api",
	"description": "amin abad property",
	"beds24_authentication": {
		"apiKey": "323eee33464f4981",
		"propKey": "323eee33464f4982"               
	},
        "ownerId": "58212",
        "propId": "103724",
        "auto_sync":"yes",
        "auto_sync_time":2,
	"rooms": {
		"236037": "47",
		"236035": "45",
		"236036": "46"
	},
       "change_price":"no",
       "apply_rooms_price_rate":"yes",
       "rooms_price_rates":{
                "236037": "1.4",
		"236035": "4",
		"236036": "1.2"
        }
}',true);
    }

    protected function _after()
    {
    }

    // tests
    public function testGetPriceRate()
    {
        $priceRate1 = \Yii::$app->Beds24Api->getPriceRate($this->_beds24_information1, $this->_roomId);
        $priceRate2 = \Yii::$app->Beds24Api->getPriceRate($this->_beds24_information2, $this->_roomId);
        $priceRate3 = \Yii::$app->Beds24Api->getPriceRate($this->_beds24_information2, null);
        $this->assertEquals(4, $priceRate1);
        // must return false if change_price is set to no in config
        $this->assertEquals(false, $priceRate2);
        // must return null if room id is not set
        $this->assertEquals(null, $priceRate3);
    }
}