<?php

class getRoomsDatesFromZhavroadTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    
    const DAY_IN_SECONDS = 86400;
    
    private $_zhavroadRoomNumber;
    private $_from;
    private $_to;
    private $_price_rate;
    
    protected function _before()
    {
        //  Hotel: Amin Abad|Single Room
        $this->_zhavroadRoomNumber = 45;
        $this->_from = '2020-03-01';
        $this->_to = '2020-03-03';
        $this->_price_rate = 1.4;
    }

    protected function _after()
    {
    }

    // tests
    /**
     * @covers Beds24Api::getRoomsDatesFromZhavroad
     */
    public function testGetRoomsDatesFromZhavroad()
    {    
        /**
         * initial value sets
         */
        $zhavroadRoomNumber = $this->_zhavroadRoomNumber;
        $from = $this->_from;
        $to = $this->_to;
        /**
         * end initial value sets
         */
        $target_id = $zhavroadRoomNumber;
        $resultQuery = [];
        for ($i = strtotime($from); $i <= strtotime($to); $i += self::DAY_IN_SECONDS) {
            $SQL = "SELECT `active`,`number`,`start_date`,`price` FROM bravo_hotel_room_dates WHERE start_date=:start_date AND target_id=:target_id";
            $resultQuery[] = Yii::$app->db_booking->createCommand($SQL)->bindValue(':start_date', date('Y-m-d', $i))->bindValue(':target_id', $target_id)->queryOne();
        }
        $result = $this->_changeArrayFormatForBeds24($resultQuery, $this->_price_rate);
        $this->assertSame([
            '20200301' => [
                "p1" => 14.29,
                "i" => '3'

            ],
            '20200302' => [
                "p1" => 7.14,
                "i" => '3'
            ],
            '20200303' => [
                "p1" => 28.57,
                "i" => '3'

            ],
        ], $result);
    }
        /**
     * 
     * @param array $array
     * @return array
     */
    private function _changeArrayFormatForBeds24(array $array, $priceRate): array {
        $arr = [];
        foreach ($array as $v) {
            $t = date('Ymd', strtotime($v['start_date']));
            if($priceRate !== false) {
                 $arr[$t]["p1"] = !is_null($priceRate) ? round((1 / $priceRate) * $v['price'], 2) :round($v['price'], 2);
            }
            $arr[$t]["i"] = $v['number'];
        }
        return $arr;
    }
}