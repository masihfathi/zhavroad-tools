<?php 
class getRoomNamesFromZhavroadOTATest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    
    private $_rooms;
    protected function _before()
    {
        // Double Room | Single Room | Triple | aminabad property
        $this->_rooms = [
            46,
            45,
            47
        ];
    }

    protected function _after()
    {
    }

    // tests
    public function testGetRoomNamesFromZhavroadOTA()
    {
         /**
         * initial value sets
         */
        $rooms = $this->_rooms;
        /**
         * end initial value sets
         */
        $countRooms = count($rooms);
        $room_values = array_values($rooms);
        $newRoomsValues = [];
        $zhavroad_room_numbers = implode(', ', $room_values);
        $SQL = 'SELECT `id`,`title` FROM `bravo_hotel_rooms` WHERE `id` in (' . trim(strip_tags($zhavroad_room_numbers)) . ')';
        $zhavroad_room_titles = \Yii::$app->db_booking->createCommand($SQL)->queryAll();
        if (empty($zhavroad_room_titles) || (count($zhavroad_room_titles) != $countRooms)) {
            throw new NotFoundHttpException('zhavroad room numbers do not match with saved room numbers in zhavroad OTA, check room numbers in zhavroad platform');
        }
        for ($i = 0; $i < $countRooms; $i++) {
            $key = array_search($zhavroad_room_titles[$i]['id'], $rooms);
            $newRoomsValues[$key] = sprintf('%s - room number(%s)', $zhavroad_room_titles[$i]['title'], $zhavroad_room_titles[$i]['id']);
        }
        $this->_arrays_are_similar([
            0 => 'Double Room - room number(46)',
            1 => 'Single Room - room number(45)',
            2 => 'Triple - room number(47)'
                ], $newRoomsValues);
    }
    
    private function _arrays_are_similar($a, $b) {
        // if the indexes don't match, return immediately
        if (count(array_diff_assoc($a, $b))) {
            return false;
        }
        // we know that the indexes, but maybe not values, match.
        // compare the values between the two arrays
        foreach ($a as $k => $v) {
            if ($v !== $b[$k]) {
                return false;
            }
        }
        // we have identical indexes, and no unequal values
        return true;
    }
}