<?php

namespace tests\unit;

use yii\caching\TagDependency;
use app\models\SyncForm;

class scheduleTest extends \Codeception\Test\Unit {

    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before() {
        
    }

    protected function _after() {
        
    }

    // tests
    public function testSchedule() {
        $db = \Yii::$app->db;
        $duration = 3600;     // cache query results for 3600 seconds.
        $result = $db->cache(function ($db) {
            $SQL = 'SELECT `hotel_id`, `beds24_room_map`,`status` FROM hotel_apis WHERE JSON_EXTRACT(`beds24_room_map`, "$.auto_sync") = :auto_sync';
            return $db->createCommand($SQL)->bindValue(':auto_sync', "yes")->queryAll();
        }, $duration, new TagDependency(['tags' => 'auto_sync_schedule']));
        $this->assertCount(2, $result);
        $value = $result[1];
        //foreach ($result as $value) {
        $beds24_information = json_decode($value['beds24_room_map'], true);
        $auto_sync_time = $beds24_information['auto_sync_time']; // auto sync time in minutes
        $this->assertSame(3, $auto_sync_time);
        $hotel_id = $value['hotel_id'];
        $this->assertSame(13, (int) $hotel_id);
        $this->assertSame('active', $value['status']);
        if ($value['status'] == 'active') {
            $this->assertSame('active', $value['status']);
            //$schedule->call(function() use($beds24_information, $hotel_id) {
            $rooms = $beds24_information['rooms'];
            foreach ($rooms as $roomId => $zhavroadRoomNumber) {
                $priceRate = \Yii::$app->Beds24Api->getPriceRate($beds24_information, $roomId);
                $p[] = $priceRate;
                $model = new SyncForm(['scenario' => SyncForm::SCENARIO_WEBHOOK_FOR_ZHAVROAD_SYNC]);
                $model->roomId = $roomId;
                $from = date('Ymd', time());
                $to = date('Ymd', strtotime('+1 year'));
                $res = $model->Beds24GetRoomDates($beds24_information, $from, $to);
                if ($res['success'] === true) {
                    $msg = \Yii::$app->Beds24Api->updateZhavroad($res['info'], $zhavroadRoomNumber, $hotel_id, $priceRate);
                    $this->assertEquals(366, count($msg));
                    \Yii::error($msg);
                } else {
                    \Yii::error($res);
                }
                //sleep(5); // 5 second sleep between each rooms set
            }
            $this->_arrays_are_similar([
            "1.4",
            "1.2",
            ".9",
            "1.6"
                ], $p);
        }
        //})->everyNMinutes($auto_sync_time);
        //}
    }

    private function _arrays_are_similar($a, $b) {
        // if the indexes don't match, return immediately
        if (count(array_diff_assoc($a, $b))) {
            return false;
        }
        // we know that the indexes, but maybe not values, match.
        // compare the values between the two arrays
        foreach ($a as $k => $v) {
            if ($v !== $b[$k]) {
                return false;
            }
        }
        // we have identical indexes, and no unequal values
        return true;
    }

}
