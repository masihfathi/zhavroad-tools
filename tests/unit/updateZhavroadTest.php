<?php 
namespace tests\unit;

class updateZhavroadTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    
    private $_data = [];
    
    private $_hotelId;
    
    private $_zhavroadRoomNumber;
    
    private $_dataCount = 0;
    
    protected function _before()
    {
         /*
        03/01/2020 - 04/30/2020  date with hotel id = 12, and room id single = 45, double = 46, triple = 47       
        ex, data of api response format:
        $res = [
            '20200226' => [
                'i' => 2 ,
                'p1' => '10.00'
            ],
            '20200227' => [
                'i' => 1,
                'p1' => '10.00'
            ],
            '20200228' => [
                'i' => 1,
                'p1' => '10.00'
            ],
            '20200229' => [
                'i' => 1,
                'p1' => '10.00'
            ],
            '20200301' => [
                'i' => 1,
                'p1' => '10.00'
            ]
        ]  
        */
        $this->_hotelId = 13;
        $this->_zhavroadRoomNumber = 50;        

        $this->_data['20200301'] = [
                'i'=> 0,
                'p1' => 12
        ];
        $this->_data['20200302'] = [
                'i'=> 1,
                'p1' => 9
        ];
        $this->_data['20200303'] = [
                'i'=> 2,
                'p1' => 8
        ];
        $this->_data['20200304'] = [
                'i'=> 2,
                'p1' => 0
        ];
        $this->_data['20200305'] = [
                'i'=> 0,
                'p1' => 0
        ];
        $this->_data['20200306'] = [
                'i'=> 0,
        ];
        $this->_dataCount = count($this->_data);
    }

    protected function _after()
    {
        
    }

    // tests
    /**
     * @covers Beds24Api::updateZhavroad
     */
    public function testUpdateZhavroad()
    {  
        
        // initial values for test         
        $apiResponse = $this->_data;
        $zhavroadRoomNumber = $this->_zhavroadRoomNumber;
        $priceRate = 2;
        $condition = [];
        // end initial values set
        
        $msg = [];
        foreach ($apiResponse as $time => $roomStatus) {
            $t = date("Y-m-d H:i:s", strtotime($time));
            $target_id = $zhavroadRoomNumber;
            if ($roomStatus['i'] > 0) {
                $number = $roomStatus['i'];
                $active = 1;                
            } elseif ($roomStatus['i'] == 0) {
                $number = null;
                $active = 0;
            }
            if (isset($roomStatus['p1']) && ($roomStatus['p1'] > 0)) {
                $beds24_price = $roomStatus['p1'];
                if($priceRate !== false){
                    $p = (!is_null($priceRate)) ? ($priceRate * $beds24_price) : $beds24_price;                    
                } else {
                    $p = null;
                }
            } else {
                $p = null;
            }
            if (is_null($p) && is_null($number)) {
                $condition[] = 1;
                $SQL = "UPDATE bravo_hotel_room_dates SET active=:active WHERE start_date=:start_date AND target_id=:target_id";
                $resultQuery = \Yii::$app->db_booking->createCommand($SQL)->bindValue(':start_date', $t)->bindValue(':target_id', $target_id)->bindValue(':active', $active)->execute();
            } elseif (is_null($p) && !is_null($number)) {
                $condition[] = 2;
                $SQL = "UPDATE bravo_hotel_room_dates SET active=:active, number=:num WHERE start_date=:start_date AND target_id=:target_id";
                $resultQuery = \Yii::$app->db_booking->createCommand($SQL)->bindValue(':start_date', $t)->bindValue(':num', $number)->bindValue(':target_id', $target_id)->bindValue(':active', $active)->execute();
            } elseif (!is_null($p) && is_null($number)) {
                $condition[] = 3;
                $SQL = "UPDATE bravo_hotel_room_dates SET price=:price, active=:active WHERE start_date=:start_date AND target_id=:target_id";
                $resultQuery = \Yii::$app->db_booking->createCommand($SQL)->bindValue(':start_date', $t)->bindValue(':price', $p)->bindValue(':target_id', $target_id)->bindValue(':active', $active)->execute();
            } elseif (!is_null($p) && !is_null($number)) {
                $condition[] = 4;
                $SQL = "UPDATE bravo_hotel_room_dates SET price=:price, active=:active, number=:num WHERE start_date=:start_date AND target_id=:target_id";
                $resultQuery = \Yii::$app->db_booking->createCommand($SQL)->bindValue(':start_date', $t)->bindValue(':num', $number)->bindValue(':price', $p)->bindValue(':target_id', $target_id)->bindValue(':active', $active)->execute();
            }
            $this->assertNotEquals(0, $resultQuery);
            if ($resultQuery == 0) {
                array_push($msg, "Row with time date of " . trim(strip_tags(date("Y-m-d", strtotime($time)))) . " and room number of $target_id dose not update");
            } else {
                array_push($msg, "Row with time date of " . trim(strip_tags(date("Y-m-d", strtotime($time)))) . " and room number of $target_id updated");
            }
        }
        $this->assertTrue($this->_arrays_are_similar([
            "Row with time date of " . date("Y-m-d", strtotime('20200301')) . " and room number of $zhavroadRoomNumber updated",
            "Row with time date of " . date("Y-m-d", strtotime('20200302')) . " and room number of $zhavroadRoomNumber updated",
            "Row with time date of " . date("Y-m-d", strtotime('20200303')) . " and room number of $zhavroadRoomNumber updated",
            "Row with time date of " . date("Y-m-d", strtotime('20200304')) . " and room number of $zhavroadRoomNumber updated",
            "Row with time date of " . date("Y-m-d", strtotime('20200305')) . " and room number of $zhavroadRoomNumber updated",
            "Row with time date of " . date("Y-m-d", strtotime('20200306')) . " and room number of $zhavroadRoomNumber updated"
        ], $msg));
        $this->assertTrue($this->_arrays_are_similar([
            3,
            4,
            4,
            2,
            1,
            1,
        ], $condition));
        $this->assertCount($this->_dataCount, $msg);
    }
    private function _arrays_are_similar($a, $b) {
        // if the indexes don't match, return immediately
        if (count(array_diff_assoc($a, $b))) {
          return false;
        }
        // we know that the indexes, but maybe not values, match.
        // compare the values between the two arrays
        foreach($a as $k => $v) {
          if ($v !== $b[$k]) {
            return false;
          }
        }
        // we have identical indexes, and no unequal values
        return true;
    }   
}