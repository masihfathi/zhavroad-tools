<?php

use yii\helpers\Url;

class ApiTestConnectionCest {

    public function testSuccessfullConnection(AcceptanceTester $I) {
        $I->amOnPage(Url::toRoute('/hotel-apis/update?id=4'));
        $I->fillField('input[name="LoginForm[username]"]', 'yii-admin');
        $I->fillField('input[name="LoginForm[password]"]', 'Majid18413');
        $I->click('login-button');
        $I->wait(1); // wait for button to be clicked        
        $I->see('Update Hotel Apis: sara','h1');
        $I->amGoingTo('Test successful api connection!');
        $I->click('#connection-test');
        $I->wait(6);
        $I->see('Connection information is correct');
    }

}
