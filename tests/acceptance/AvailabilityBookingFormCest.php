<?php

use yii\helpers\Url;

class AvailabilityBookingFormCest {

    public function uploadAvailabilityFile(AcceptanceTester $I)
    {
        $I->amOnPage(Url::toRoute('/site/availability'));
        $I->see('Availability booking form', 'h2');
        $I->amGoingTo('try to upload availability csv file');
        $I->fillField(['name' => 'AvailabilityCsvUploadForm[apiKey]'], '323eeee7-464f-4981-a22f-1ac5e3feff4f');
        $I->attachFile('input[id="availabilitycsvuploadform-file"]', 'csv-format-example.csv');
        $I->click('button[name="availability-button"]');
        $I->wait(1); // wait for file to be uploaded
        $I->see('Row with time date of 2019-12-01');
    }
    public function roomNumbersAreIncorrect(AcceptanceTester $I)
    {
        $I->amOnPage(Url::toRoute('/site/availability'));
        $I->see('Availability booking form', 'h2');
        $I->amGoingTo('try to upload csv file with incorrect room numbers');
        $I->fillField(['name' => 'AvailabilityCsvUploadForm[apiKey]'], '323eeee7-464f-4981-a22f-1ac5e3feff4f');
        $I->attachFile('input[id="availabilitycsvuploadform-file"]', 'csv-room-numbers-incorrect.csv');
        $I->click('button[name="availability-button"]');
        $I->wait(1); // wait for file to be uploaded
        $I->see('room numbers are incorrect');
    }
    public function uploadNotPermittedFile(AcceptanceTester $I)
    {
        $I->amOnPage(Url::toRoute('/site/availability'));
        $I->see('Availability booking form', 'h2');
        $I->amGoingTo('upload not permitted file');
        $I->fillField(['name' => 'AvailabilityCsvUploadForm[apiKey]'], '323eeee7-464f-4981-a22f-1ac5e3feff4f');
        $I->attachFile('input[id="availabilitycsvuploadform-file"]', 'csv-format-example.php');
        $I->click('button[name="availability-button"]');
        $I->wait(1); // wait for file to be uploaded
        $I->see('Only files with these extensions are allowed: csv.');
    }
    public function signUpWithWrongCredentialsApiKeyForm(AcceptanceTester $I)
    {
        $I->amOnPage(Url::toRoute('/site/availability'));
        $I->see('Availability booking form', 'h2');
        $I->amGoingTo('signup with wrong credentials api key field');
        $I->fillField(['name' => 'AvailabilityCsvUploadForm[apiKey]'], '323eeee7-464f-4981-a22f-1ac5e3feff45');
        $I->attachFile('input[id="availabilitycsvuploadform-file"]', 'csv-format-example.csv');
        $I->click('button[name="availability-button"]');
        $I->wait(1); // wait for file to be uploaded
        $I->see('Api key is not correct');
    }
}
