<?php

use yii\helpers\Url;

class LoginCest
{
    public function loginFailedTest(AcceptanceTester $I)
    {
        $I->amOnPage(Url::toRoute('/site/login'));
        $I->see('Login', 'h1');

        $I->amGoingTo('try to login with incorrect credentials');
        $I->fillField('input[name="LoginForm[username]"]', 'admin');
        $I->fillField('input[name="LoginForm[password]"]', 'admin');
        $I->click('login-button');
        $I->wait(1); // wait for button to be clicked

        $I->expectTo('Incorrect username or password.');
    }
}
