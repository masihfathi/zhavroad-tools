<?php

use yii\db\Migration;

/**
 * Class m191224_091532_hotel_apis
 */
class m191224_091532_hotel_apis extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('hotel_apis', [
            'hotel_apis_id' => $this->primaryKey(),
            'hotel_id' => $this->string()->notNull(),
            'hotel_name' => $this->string(),
            'api_key' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('hotel_apis');
    }
}
