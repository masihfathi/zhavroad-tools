<?php

use yii\db\Migration;

/**
 * Class m200130_080406_add_beds24_room_map_to_hotel_apis
 */
class m200130_080406_add_beds24_room_map_to_hotel_apis extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('hotel_apis', 'beds24_room_map', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('hotel_apis', 'beds24_room_map');
    }
}
