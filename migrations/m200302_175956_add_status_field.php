<?php

use yii\db\Migration;

/**
 * Class m200302_175956_add_status_field
 */
class m200302_175956_add_status_field extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('hotel_apis', 'status', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('hotel_apis', 'status');
    }
}
